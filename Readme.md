# Wordpress Ansible Role
-----------------------------------

This Ansible role will set up a new wordpress instance. 

What it does: 
1. Create a new Nginx virtual host file in /etc/nginx/vhosts/my-site-name.conf with the vhost.conf template 
2. Create a new directory at /var/www/my-site-name/ 
3. Download latest Wordpress archive and extract it to the above directory
4. Create the database name and user for this site
5. Transfer the wp-config.php file with the new database details
6. Change owner of /var/www/my-site-name with the Nginx user (default: nginx) (recursive)
7. Apply default permissions 755 for directories and 644 for files 

What it does NOT:
* Install Nginx (will be added as a separate role in the future)
* Install MariaDB (will be added as a separate role in the future)

> playbook.yml:
~~~~
---
- name: Install WordPress
  hosts: all
  become: true
  roles: 
    - ansible-wordpress-role
~~~~

[Arjon Bujupi](https://a.bujupi.me)
